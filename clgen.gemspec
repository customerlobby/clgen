# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'clgen/version'

Gem::Specification.new do |spec|
  spec.name          = "clgen"
  spec.version       = Clgen::VERSION
  spec.authors       = ["David Lains"]
  spec.email         = ["dlains@customerlobby.com"]
  spec.summary       = %q{Customer Lobby Code Generator}
  spec.description   = %q{Generate code for API Gems, Mappers, Fetchers and Direct Connect Setup rake files.}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.5"
  spec.add_development_dependency "rake"
  spec.add_development_dependency('rspec', '~> 3.1')
  spec.add_runtime_dependency('activesupport')
  spec.add_runtime_dependency('gli')
end
