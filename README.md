# Clgen

`clgen` if the Customer Lobby code generation tool. It can be used to create API Client gems, Direct
Connect Mappers, Fetchers and Setup tasks.

## Installation

Add this line to your application's Gemfile:

    gem 'clgen'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install clgen

## Usage

Each type of output has a separate sub command.

There are two options that can be used with any of the code generators.
* -d, --destination Provide a destination path for the code to be written to.
* -v, --verbose Provice verbose output as the generator runs

### Gem Creation

```bash
clgen [options] gem gem_name
```

The `gem_name` argument should be the standard ruby snake-case version of the name for your gem. If you
want a gem named FooBar you should provide 'foo_bar' on the command line. The main library file in the
output will be 'foo_bar.rb' and the module will be 'FooBar'.

If you are running the code from a cloned copy of the repo use the following syntax:

```bash
bundle exec bin/clgen [options] gem gem_name
```

To specify a directory to ouput the new gem to use the `-d` global option.

```bash
bundle exec bin/clgen -d ~/Work gem gem_name
```

### Direct Connect System Setup

```bash
clgen [options] setup [type] setup_name display_name
```

The `setup_name` will be the file name and also be used in the name of the rake task. The `display_name`
will be displayed in the UI when people choose their workflow system, so use the name user would be
most familiar with. If there are spaces in the `display_name` be sure to surround it with quotes.

The `type` flag determines the kind of workflow system being created. This can be either 'desktop' or 'api'.
This flag is set to 'desktop' by default.

If you are running the code from a cloned copy of the repo use the following syntax:

```bash
bundle exec bin/clgen [options] gem gem_name
```

### Mapper Creation

```bash
clgen [options] mapper mapper_name
```

The `mapper_name` will be used to create a directory and the file names and should be the standard ruby
snake-case version of the name. As with the gem creator, if you want a mapper named FooBar, enter 'foo_bar'
on the command line. The output directory and file names will contain 'foo_bar'. The class names will be
FooBar.

If you are running the code from a cloned copy of the repo use the following syntax:

```bash
bundle exec bin/clgen [options] gem gem_name
```

## Contributing

1. Fork it ( http://github.com/<my-github-username>/clgen/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
