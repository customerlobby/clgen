module Clgen

  class MapperCreator < BaseCreator

    # For ERB to have access to the attributes.
    def get_binding
      binding()
    end

    # Get the path to the gem template.
    # @return [String] the full path to the mapper template.
    def template_path
      return "#{base_template_path}/mapper"
    end

  end

end
