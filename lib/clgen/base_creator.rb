module Clgen
  
  class BaseCreator
    attr_accessor :name, :class_name, :destination

    DIRNAME_REPLACE_MARKER = 'replace_dir'
    FILENAME_REPLACE_MARKER = 'replace'

    def initialize(global_options, options, args)
      @global_options = global_options
      @options = options
      @args = args
      create_logger(global_options)
      create_names(args)
      create_destination(global_options)
    end

    # Create the requested code. Gets the template path from the subclass.
    def create
      create_directory(@destination)

      # Cycle through the files / directories in 'templates/setup' and produce the new rake task.
      Dir.glob("#{template_path}/**/*", File::FNM_DOTMATCH).each do |path|
        # Copy non .erb files to destination
        if path.end_with?('.erb')
          content = read_template(path)
          renderer = ERB.new(content)
          write_file(renderer, destination_file_path(path))
        else
          if File.directory?(path)
            FileUtils.mkdir_p(path)
          else
            FileUtils.copy(path, @destination)
          end
        end
      end
      @logger.info("Completed generating #{class_name} at #{@destination}")
    end

    private

    # Read the template file from the path specified.
    # @param [String] path the full path to the template file to read.
    # @return [String] the contents of the template file.
    def read_template(path)
      @logger.debug("Reading template file: #{path}")
      result = ''
      File.open(path, 'r') do |file|
        while line = file.gets
          result += line
        end
      end
      return result
    end

    # Write a file after rendering any ERB data in the template.
    # @param [ERB] renderer the ERB data to be rendered.
    # @param [String] file_name the full path and file name to write to.
    def write_file(renderer, file_name)
      @logger.debug("Writing destination file: #{file_name}")
      File.open(file_name, 'w+') do |file|
        file.write(renderer.result(self.get_binding))
      end
    end

    # Determine the destination file path. This takes into account subdirectories within
    # the template and ensures the destination directory gets created.
    # @param [String] path the path to the template file.
    # @return [String] the destination path to write the file to.
    def destination_file_path(path)
      file_name = destination_file_name(path)
      dir_name  = destination_dir_name(path)
      create_directory(dir_name)
      @logger.debug("Destination file path: #{dir_name}#{file_name}")
      return "#{dir_name}#{file_name}"
    end

    # Determine the destination directory name. Replaces any directory names that match the
    # 'replace_dir' marker with the 'name' passed to the generator.
    # @param [String] path the full path of the template file being processed.
    # @return [String] the final destination directory for the template file.
    def destination_dir_name(path)
      subdir = File.dirname(path).sub(template_path, '')
      return @destination if subdir == '/'
      # Remove leading / from subdir.
      subdir = subdir.sub('/','')
      # Replace the directory name replace marker in the dir name with the actual gem name.
      subdir = subdir.sub(DIRNAME_REPLACE_MARKER, name)
      return "#{@destination}#{subdir}/"
    end

    # Determine the destination file name. Replaces any file names that match the 'replace'
    # marker with the 'name' passed into the generator.
    # @param [String] path the full path to the template file to be processed.
    # @return [String] the final file name to use with any replacement done and '.erb' removed.
    def destination_file_name(path)
      file = File.basename(path)
      if file.start_with?(FILENAME_REPLACE_MARKER)
        file = file.sub(FILENAME_REPLACE_MARKER, name)
      end
      file = file.sub('.erb', '')
      return file
    end

    # Create a directory while generating code. If the directory name matches the string
    # 'replace_dir' the name will be replaced with the 'name' argument.
    # @param [String] path the full path to the directory to be created.
    def create_directory(path)
      if path.include?(DIRNAME_REPLACE_MARKER)
        path = path.sub(DIRNAME_REPLACE_MARKER, name)
      end
      FileUtils.mkdir_p path unless Dir.exist?(path)
      @logger.debug("Create Directory: #{path}")
    end

    # Get the base path to the templates.
    # @return [String] the full path to the template directory.
    def base_template_path
      return File.expand_path "#{File.dirname(__FILE__)}/templates"
    end

    # Create the names to be used in template processing.
    # @param [Array] the arguments passed into the process.
    def create_names(args)
      @name = args[0]
      @class_name = @name.camelize
      @logger.debug("File Name: #{@name}")
      @logger.debug("Class Name: #{@class_name}")
    end

    # Create the destination directory information.
    # @param [Hash] global_options the global options passed into the process.
    def create_destination(global_options)
      @destination = "#{global_options[:d]}/#{name}/"
      @logger.debug("Output Destination: #{@destination}")
    end

    # Create the logger. This is used to output simple information as the process runs.
    # @param [Hash] global_options check the global options for the verbose flag.
    def create_logger(global_options)
      @logger = Logger.new(STDOUT)
      @logger.formatter = proc do |severity, datetime, progname, msg|
        "#{msg}\n"
      end
      if global_options[:v]
        @logger.level = Logger::DEBUG
      else
        @logger.level = Logger::INFO
      end
    end
  end

end
