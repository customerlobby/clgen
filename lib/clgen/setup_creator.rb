module Clgen

  class SetupCreator < BaseCreator
    attr_accessor :display_name, :type

    def initialize(global_options, options, args)
      super
      @display_name = args[1]
      @type = options[:t]
    end

    # For ERB to have access to the attributes.
    def get_binding
      binding()
    end

    # Get the path to the setup template.
    # @return [String] the full path the the setup template.
    def template_path
      return "#{base_template_path}/setup"
    end

  end

end
