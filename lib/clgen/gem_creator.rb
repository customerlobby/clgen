module Clgen

  class GemCreator < BaseCreator

    # For ERB to have access to the attributes.
    def get_binding
      binding()
    end

    # Get the path to the gem template.
    # @return [String] the full path to the gem template.
    def template_path
      return "#{base_template_path}/gem"
    end

  end

end
